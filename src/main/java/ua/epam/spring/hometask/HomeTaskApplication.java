package ua.epam.spring.hometask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//@ComponentScan("ua.epam.spring.hometask")
public class HomeTaskApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(HomeTaskApplication.class, args);
    }
}
