package ua.epam.spring.hometask.service.impl;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import java.util.List;


public class DiscountStrategy {

    private List<Event> events;

    public DiscountStrategy(List<Event> events) {
        this.events = events;
    }

    public List<Event> getEvents() {
        return events;
    }


    public double getBirthdayDiscount(User user, Ticket ticket ) {

        for (Event event : this.getEvents()) {
            if (user.getBirthDate().equals(ticket.getDateTime())) {
                double birthDayPrice = event.getBasePrice() * 0.95;
                return  birthDayPrice; //TODO: ehelyett vmi mást
            }

        }
        return 0;
    }

    public double getFiftyPercentDiscountForEveryTenthTicket(User user, Ticket ticket) {



        return 0;
    }

} //TODO: kidolgozni!
