package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private DiscountService discountService;

    @Override
    public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user, @Nonnull Long seats) {
        double price = event.getBasePrice() * seats;
        double discount = discountService.getDiscount(user, event, dateTime, seats);
        return price - discount;

    }

    @Override
    public void bookTickets(@Nonnull Set<Ticket> tickets, User user) {
        for (Ticket ticket : tickets) {
            user.getTickets();
            tickets.add(ticket);
        }
    }

    @Nonnull
    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime, User user) {
        return user.getTickets().stream().filter(ticket -> ticket.getEvent().getId().equals(event.getId())).collect(Collectors.toSet());
    }
}
