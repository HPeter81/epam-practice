package ua.epam.spring.hometask.service.Exceptions;

public class UserDoesNotExistsException extends  Exception {

    public UserDoesNotExistsException() {
        super();
    }

    public UserDoesNotExistsException(final String message) {
        super(message);
    }

    public UserDoesNotExistsException(final Throwable cause) {
        super(cause);
    }

    public UserDoesNotExistsException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
