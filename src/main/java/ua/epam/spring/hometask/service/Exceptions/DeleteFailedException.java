package ua.epam.spring.hometask.service.Exceptions;

public class DeleteFailedException extends  Exception {

    public DeleteFailedException() {
        super();
    }

    public DeleteFailedException(final String message) {
        super(message);
    }

    public DeleteFailedException(final Throwable cause) {
        super(cause);
    }

    public DeleteFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
