package ua.epam.spring.hometask.service.Exceptions;

public class UserListIsEmptyException extends Exception {

    public UserListIsEmptyException() {
        super();
    }

    public UserListIsEmptyException(final String message) {
        super(message);
    }

    public UserListIsEmptyException(final Throwable cause) {
        super(cause);
    }

    public UserListIsEmptyException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
