package ua.epam.spring.hometask.service.Exceptions;

public class SaveFailedException extends Exception {

    public SaveFailedException() {
        super();
    }

    public SaveFailedException(final String message) {
        super(message);
    }

    public SaveFailedException(final Throwable cause) {
        super(cause);
    }

    public SaveFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
