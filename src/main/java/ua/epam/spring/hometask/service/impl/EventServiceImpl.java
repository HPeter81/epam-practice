package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.service.EventService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventDao eventDao;

    @Nullable
    @Override
    public List<Event> getNextEvents(LocalDate to) {
        return eventDao.getNextEvents(to);
    }

    @Nullable
    @Override
    public List<Event> getForDateRange(LocalDate from, LocalDate to) {
        return eventDao.getForDateRange(from, to);
    }

    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return eventDao.getByName(name);
    }

    @Override
    public void save(@Nonnull Event event) {
       eventDao.save(event);
    }

    @Override
    public void remove(@Nonnull Event event) {
        eventDao.remove(event);

    }

    @Override
    public Event getById(@Nonnull Long id) {
        return eventDao.getById(id);
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return eventDao.getAll();
    }
}
