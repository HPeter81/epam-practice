package ua.epam.spring.hometask.domain;

public enum Role {
    ADMIN("Admin"), USER("User");

    private String role;

    Role(String role) {
        this.role = role;
    }

    public String role() {
        return role;
    }
}
