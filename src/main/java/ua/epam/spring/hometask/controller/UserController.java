package ua.epam.spring.hometask.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import ua.epam.spring.hometask.domain.Role;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.Exceptions.SaveFailedException;
import ua.epam.spring.hometask.service.Exceptions.UserDoesNotExistsException;
import ua.epam.spring.hometask.service.Exceptions.UserListIsEmptyException;
import ua.epam.spring.hometask.service.UserService;

import java.util.*;


@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/firstname", method = RequestMethod.GET)
    public User findByFirstName(String firstName) throws UserDoesNotExistsException {

          User user = userService.getUserByFirstName(firstName);
          if(user.getFirstName() == null) {
              throw new UserDoesNotExistsException();

          }

        return user;
    }

    @RequestMapping(value = "/listuser", method = RequestMethod.GET)
    public Collection<User> getAllUsers() throws UserListIsEmptyException {
       Collection<User> users = userService.getAll();
       if (users == null ) {
           throw new UserListIsEmptyException();
       }
       return users;
    }

    @RequestMapping(value = "/finduserbyemail", method = RequestMethod.GET)
    public User findByEmail(@RequestParam String email) throws UserDoesNotExistsException {
        User user = userService.getUserByEmail(email);
        if (user.getEmail() == null) {
            throw new UserDoesNotExistsException();
        }
        return user;
    }

    @RequestMapping(value="/finduserbyid", method = RequestMethod.GET)
    public User findUserById(Long id) throws UserDoesNotExistsException{
        User user = userService.getById(id);
        if (user.getId() == null) {
            throw new UserDoesNotExistsException();
        }
        return user;

    }

    @RequestMapping(value = "/saveuser", method = RequestMethod.GET)
    public String saveUser() {
       return "saveuser";
    }

    @RequestMapping(value = "/saveuserresult", method = RequestMethod.POST)
    public String saveUserResult(@RequestParam(value = "user")Long id, String firstName, String lastName, String email,Role role, Date birthDate) throws SaveFailedException {
       User user = new User(id, firstName, lastName, email, role, birthDate );
          userService.save(user);
        if (user.getId() == null) {
            throw new SaveFailedException();
        }
       return "save successful";
    }

  /*  @RequestMapping(value = "/deleteuser", method = RequestMethod.GET)
    public String deleteUser() {
        return "deleteuser";
    } */

    @RequestMapping(value = "/deleteuser/{id}", method = RequestMethod.DELETE)
    public String deleteUserResult(@PathVariable("id") Long id) {
        User user = userService.getById(id);
            userService.remove(user);
        if (user.getId() != null) {
            throw new DataIntegrityViolationException("Delete unsuccessful!");
        }
        return "delete successful";
    }

}
