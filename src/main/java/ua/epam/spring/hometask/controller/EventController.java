package ua.epam.spring.hometask.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ua.epam.spring.hometask.service.EventService;

@Controller
public class EventController {

   // private static final Logger logger = LoggerFactory.getLogger(EventController.class);

    private EventService eventService;

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}
