package ua.epam.spring.hometask.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ua.epam.spring.hometask.service.DiscountService;

@Controller
public class DiscountController {

   // private static final Logger logger = LoggerFactory.getLogger(DiscountController.class);

    private DiscountService discountService;

    @Autowired
    public void setDiscountService(DiscountService discountService) {
        this.discountService = discountService;
    }
}
