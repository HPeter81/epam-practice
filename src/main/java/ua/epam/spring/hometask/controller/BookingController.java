package ua.epam.spring.hometask.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ua.epam.spring.hometask.service.BookingService;

@Controller
public class BookingController {

   // private static final Logger logger = LoggerFactory.getLogger(BookingController.class);

    private BookingService bookingService;

    @Autowired
    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }
}
