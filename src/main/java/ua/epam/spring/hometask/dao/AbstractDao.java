package ua.epam.spring.hometask.dao;

import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.DomainObject;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface AbstractDao < T extends DomainObject> {

    public void save(@Nonnull T object);
    public void remove(@Nonnull T object);
    public T getById(@Nonnull Long id);
    public @Nonnull Collection<T> getAll();
}
