package ua.epam.spring.hometask.dao.impl;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.AuditoriumDao;
import ua.epam.spring.hometask.domain.Auditorium;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Component
public class AuditoriumDaoImpl implements AuditoriumDao {

    private Map<String, Auditorium> auditoriums = new HashMap<>();

    InputStream inputStream;

    public void getPropertyValues() {

        try {
            Properties properties = new Properties();
            String propertyFileName = "smallhall.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propertyFileName);

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("File not found!");
            }

            Set<Long> vipSeats = new HashSet<>();

            vipSeats.add(Long.valueOf(properties.getProperty("smallhallVipSeats")));
            vipSeats.add(Long.valueOf(properties.getProperty("middlehallVipSeats")));
            vipSeats.add(Long.valueOf(properties.getProperty("greathallVipSeats")));

            auditoriums.put(properties.getProperty("smallhallName"), new Auditorium(properties.getProperty("smallhallName"),
                    Long.valueOf(properties.getProperty("smallhallNumberOfSeats")), vipSeats));
            auditoriums.put(properties.getProperty("middlehallName"), new Auditorium(properties.getProperty("middlehallName"),
                    Long.valueOf(properties.getProperty("middlehallNumberOfSeats")), vipSeats));
            auditoriums.put(properties.getProperty("greathallName"), new Auditorium(properties.getProperty("greathallName"),
                    Long.valueOf(properties.getProperty("greathallNumberOfSeats")), vipSeats));


        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Auditorium getByName(String name) {
        return auditoriums.get(name);
    }

    @Override
    public Set<Auditorium> getAll() {return new HashSet<>(auditoriums.values());
    }
}
