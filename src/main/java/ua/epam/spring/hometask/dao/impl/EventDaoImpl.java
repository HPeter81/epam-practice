package ua.epam.spring.hometask.dao.impl;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class EventDaoImpl implements EventDao {

    private static Map<Long, Event> events = new HashMap<>();

    private static NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();


    static {

        String time1 = "2018-06-10 20:00";
        String time2 = "2018-06-15 20:00";
        String time3 = "2018-06-20 20:00";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        LocalDateTime formattedTime1 = LocalDateTime.parse(time1, formatter);
        LocalDateTime formattedTime2 = LocalDateTime.parse(time2, formatter);
        LocalDateTime formattedTime3 = LocalDateTime.parse(time3, formatter);



        TreeSet<LocalDateTime> airdates = new TreeSet<>();

        airdates.add(formattedTime1);
        airdates.add(formattedTime2);
        airdates.add(formattedTime3);

        auditoriums.put(formattedTime1, new Auditorium());
        auditoriums.put(formattedTime2, new Auditorium());
        auditoriums.put(formattedTime3, new Auditorium()); //TODO: auditorium paraméterek?



        events.put(1L, new Event("Action movie", airdates, 800, EventRating.LOW, auditoriums));
        events.put(2L, new Event("Romantic movie", airdates, 1000, EventRating.MID, auditoriums));
        events.put(3L, new Event("Action movie", airdates, 1200, EventRating.HIGH, auditoriums));
    }



    @Nullable
    @Override
    public List<Event> getNextEvents(LocalDate to) {
        List<Event> eventFromNowToDateList = new ArrayList<>();
        for (Event event : events.values()) {
            if (event.airsOnDates(LocalDate.now(), to)) {
                eventFromNowToDateList.add(event);
                return eventFromNowToDateList;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public List<Event> getForDateRange(LocalDate from, LocalDate to) {
        List<Event> eventForPeriodList = new ArrayList<>();
        for (Event event : events.values()) {
            if (event.airsOnDates(from, to)) {
                eventForPeriodList.add(event);
                return eventForPeriodList;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return events.get(name);
    }

    @Override
    public void save(@Nonnull Event event) {
        Long id = Math.incrementExact(3L);
        event.setId(id);
        events.put(id, event);
    }

    @Override
    public void remove(@Nonnull Event event) {
        events.remove(event.getId());
    }

    @Override
    public Event getById(@Nonnull Long id) {
        return events.get(id);
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return events.values();
    }
}
