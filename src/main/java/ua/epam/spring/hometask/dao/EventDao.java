package ua.epam.spring.hometask.dao;

import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.Event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.List;



public interface EventDao extends AbstractDao<Event> {

    public @Nullable List<Event> getNextEvents(LocalDate to);

    public @Nullable List<Event> getForDateRange(LocalDate from, LocalDate to);

    public @Nullable Event getByName(@Nonnull String name);
}
