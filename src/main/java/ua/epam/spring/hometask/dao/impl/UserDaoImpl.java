package ua.epam.spring.hometask.dao.impl;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.UserDao;
import ua.epam.spring.hometask.domain.Role;
import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import java.util.*;

@Component
public class UserDaoImpl implements UserDao {

    private static Map<Long, User> users = new HashMap<>();

    static {
        users.put(1L, new User(1L, "Admin", "Admin", "admin@admin.com", Role.ADMIN, new Date(1970-1-1)));
        users.put(2L, new User(2L, "User", "User", "users@users.com", Role.USER,  new Date(1990-10-10)));
    }

    @Override
    public User getUserByEmail(@Nonnull String email) {
         for (User user : users.values()) {
            if (email.equals(user.getEmail())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User getUserByFirstName(String firstName) {
        for (User user : users.values()) {
            if (firstName.equals(user.getFirstName())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void save(@Nonnull User user) {
        Long id = Math.incrementExact(2L);
        user.setId(id);
        users.put(id, user);
    }

    @Override
    public void remove(@Nonnull User user) {
        users.remove(user.getId());
    }

    @Override
    public User getById(@Nonnull Long id) {
        return users.get(id);
    }

    @Nonnull
    @Override
    public Collection<User> getAll() {
        return users.values();
    }
}
