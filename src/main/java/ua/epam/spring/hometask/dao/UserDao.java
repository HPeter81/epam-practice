package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;

public interface UserDao extends AbstractDao<User> {

    public User getUserByEmail(@Nonnull String email);

    public User getUserByFirstName(String firstName);
}
