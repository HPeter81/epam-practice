package ua.epam.spring.hometask.dao;

import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.domain.Auditorium;

import java.util.Set;


public interface AuditoriumDao {

    public Auditorium getByName(String name);
    public Set<Auditorium> getAll();

}
