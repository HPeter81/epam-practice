package ua.epam.spring.hometask;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ua.epam.spring.hometask.service.Exceptions.*;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    protected Logger logger;

    public ExceptionHandlingControllerAdvice() {
        logger = LoggerFactory.getLogger(getClass());
    }

    //@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Data integrity violation")
    @ExceptionHandler(DeleteFailedException.class)
    public String conflict() {
        logger.error("Delete failed!");
        final String response = "Delete failed!";
        return response;
    }

    @ExceptionHandler(SaveFailedException.class)
    public String saveConflict() {
        logger.error("Save failed!");
        final String response = "Save failed!";
        return response;
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public String userExistsConflict() {
        logger.error("User Already Exists!");
        final String response = "User Already Exists!";
        return response;
    }

    @ExceptionHandler(UserDoesNotExistsException.class)
    public String userDoesNotExistsConflict() {
        logger.error("User Does Not Exists!");
        final String response = "User Does Not Exists!";
        return response;
    }

    @ExceptionHandler(UserListIsEmptyException.class)
    public String userListIsEmptyConflict() {
        logger.error("User List is Empty!");
        final String response = "User List is Empty!";
        return response;
    }

}
