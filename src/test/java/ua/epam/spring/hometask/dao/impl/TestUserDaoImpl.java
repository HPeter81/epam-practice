package ua.epam.spring.hometask.dao.impl;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ua.epam.spring.hometask.domain.Role;
import ua.epam.spring.hometask.domain.User;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;

public class TestUserDaoImpl {


    private static final Long ID = 1L;
    private static final String FIRST_NAME = "Admin";
    private static final String LAST_NAME = "Admin";
    private static final String EMAIL_ADDRESS = "admin@admin.com";
    private static final Role ROLE = Role.ADMIN;
    private static final Date BITRHDATE = new Date(1970 - 1 - 1);


    private UserDaoImpl userDaoImpl;
    private User user;
    private static Map<Long, User> users = new HashMap<>();

    @Before
    public void setUp() {
        userDaoImpl = new UserDaoImpl();
        users.put(ID, new User(ID, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, ROLE, BITRHDATE));
    }


    @Test
    public void testMethodShouldFindTheUserByEmailAddress() {
        user = users.get(ID);
        Assert.assertEquals(user.getEmail(), EMAIL_ADDRESS);
    }

    @Test
    public void testMethodShouldSaveNewUser() {
        Long id = Math.incrementExact(ID);
        users.put(id, new User(id, "John", "Doe", "johndoe@johndoe.com", Role.USER, new Date(1992 - 2 - 2)));
        user = users.get(id);
        long newId = user.getId();
        Assert.assertEquals(newId, 2);
        Assert.assertEquals(users.size(), 2);
    }

    @Test
    public void testMethodShouldRemoveUser() {
        user = users.get(ID);
        users.remove(user.getId());
        Assert.assertThat(users.keySet(), not(hasItem(user.getId())));
    }

    @Test
    public void testMethodShouldFindTheUserById() {
        user = users.get(ID);
        Assert.assertEquals(user.getId(), ID);
    }

    @Test
    public void testMethodShouldGetAllUsers() {
        Assert.assertEquals(users.size(), 2);

    }
}
